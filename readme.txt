============================================================================================================
START readme
============================================================================================================

This file focuses more on the details of the data package. For 'administrative' metadata on this data,
please view the metadata file within the parent folder of this data package (where this readme file
is also found).


============================================================================================================
## General
============================================================================================================

+ Author(s): Yuqi Gao
+ Project: Estimating the impact of low temperature on African swine fever virus transmission through contaminated environments
+ Contact: yuqi.gao@wur.nl


============================================================================================================
## Title
============================================================================================================

Data underlying the publication [Estimating the impact of low temperature on African swine fever virus transmission through contaminated environments] 


============================================================================================================
## Methods
============================================================================================================

# Introduction

African Swine Fever Virus (ASFV) is the cause of an infectious disease in pigs, which is difficult to control. Long viability of ASFV has been shown for several contaminated materials, especially under low temperature. Therefore, when pigs are exposed to a contaminated environment, new infections could occur without the presence of infectious individuals. For example, a contaminated, poorly washed, empty livestock vehicle poses a risk to the next load of pigs. A quantitative stochastic environmental transmission model was applied to simulate the change in environmental contamination levels over time and calculate the epidemic parameters through exposure-based estimation. Due to the lack of experimental data on environmental transmission at low temperatures, we performed a non-linear fit of the decay rate parameter with temperature based on a literature review. Eventually, 16 scenarios were constructed for different temperature (at 20°C, 10°C, 0°C, or -10°C) and duration of empty periods (1, 3, 5, or 7 days) after the environment had been contaminated. We quantified the variation in the contamination level of the environment over time and the probability of newly added recipients getting infected when exposed to the environment after the empty period. As a result, the transmission rate parameter for ASFV in pigs was estimated to be 1.53 (0.90, 2.45) day-1, the decay rate parameter to be 1.02 (0.73, 1.47) day-1 (at 21°C), and the excretion rate parameter to be 2.70 (2.51, 3.02) day-1. Without washing and disinfection, the environment required 9, 14, 24, 54 days to reach a low probability of causing at least one new case (<0.005) at 20°C, 10°C, 0°C, -10°C, respectively. In addition, the method proposed in this paper enables assessment of the effect of washing and disinfection on ASFV environmental transmission. We conducted this study to better understand how the viability of ASFV at different temperatures could affect the infectivity in environmental transmission and to improve risk assessment and disease control strategies.


# Measurements and data collection

A modelling approach that integrates direct and environmental transmission is used to quantify the expected effects of low temperatures on ASF transmission through contaminated environments. To construct the model, it is essential to calculate three parameters: (i) μ(T) is the decay rate parameter as a function of temperature (T) in degree Celcius, because the viability of ASFV in the environment  varies over  temperatures, (ii)  φ is the excretion rate parameter, which describes the excretion of virus from one pig, and (iii) β is the transmission rate parameter, which describes the numbers of infectious contacts through environment per time unit per infectious pig. 

We applied analysis on experimental data from 2 publish paper:

-- Olesen, A. S., Lohse, L., Boklund, A., Halasa, T., Belsham, G. J., Rasmussen, T. B., & Bøtner, A. (2018). Short time window for transmissibility of African swine fever virus from a contaminated environment. Transboundary and Emerging Diseases, 65(4), 1024-1032. https://doi.org/10.1111/tbed.12837 
-- Olesen, A. S., Lohse, L., Boklund, A., Halasa, T., Gallardo, C., Pejsak, Z., Belsham, G. J., Rasmussen, T. B., & Bøtner, A. (2017). Transmission of African swine fever virus from infected pigs by direct contact and aerosol routes. Veterinary Microbiology, 211, 92-102. https://doi.org/10.1016/j.vetmic.2017.10.004 

Since the original experiments were performed at room temperature only (21°C), we extrapolated the values of the μ at other temperatures from the published half-life values of the ASFV by performing a non-linear fit. Finally, we predicted the effect of temperature on the environmental transmission of ASF based on the above parameters in the scenario study. The model is implemented with Mathematica. All calculations are accurate to two decimal places.


============================================================================================================
## FolderStructure
============================================================================================================

There is only one parent directory present containing all files.


============================================================================================================
## FolderContents
============================================================================================================

- parent_folder/
codebook.csv          -         explanation of the variables used in the codes
direct_L5.csv         -         the new infections occurrence (with the assumption of 5-day latent period) through the direct transmission (Olesen et al., 2017)
envi_L5.csv           -         the new infections occurrence (with the assumption of 5-day latent period) through the environmental transmission (Olesen et al., 2018)
notebook_ASFV.nb      -         the processing and analysis notebook performed in Mathematica.


============================================================================================================
## Software
============================================================================================================

SoftwareRequired: 

Wolfram Mathematica 13.2


============================================================================================================
## FileFormats
============================================================================================================

+ .nb 
+ .csv


============================================================================================================
## CodeBook
============================================================================================================

Please view codebook.csv (where this readme file is also found) for documentation of abbreviations, 
column names, datapoints, etc. The file codebook.csv uses the columns:

+ index = a number used to distinguish the different entries.
+ code = the abbreviation, variable / data / column name used.
+ used = location where the code is used. (filename, foldername, columnname, datapoint, protocol, etc.).
+ meaning = the literal meaning of the code. (e.g., fully written out abbreviation)
+ represents = what the code represents in terms of data or usage. (e.g., units of measurements, 
coding used, more in depth explanation)

Note that this file is ';' delimited. To avoid possible confusion and inconsistencies, sentences within 
cells do not contain reading symbols as comma's or semicolons. 



============================================================================================================
END readme
============================================================================================================
